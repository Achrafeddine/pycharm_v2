Abbildung 1: Softwareprojektstrukturplan 10
Abbildung 2: Ideen Brainstorming 12
Abbildung 3: Funktionale Anforderungen 13-14
Abbildung 4: Projektplan 15
Abbildung 5: Ideen Brainstorming 15
Abbildung 6: PHP 17
Abbildung 7: XAMPP Control Panel 18
Abbildung 8: MySQL Workbench 19
Abbildung 9: HTML 19
Abbildung 10: Bootstrap 20
Abbildung 11: JavaScript (JS) 20
Abbildung 12: Kommodo Edit 21
Abbildung 13: Gitlab 21
Abbildung 14: CSS 22
Abbildung 15: Schriftvarianten der Nunito-Sans-Schriftfamilie 23
Abbildung 16: Nunito-Sans-Schriftfamilie CSS-Code 23
Abbildung 17: Die Farben 24
Abbildung 18: Icons-Beispiel aus der Webanwendung 25
Abbildung 18.1: Icons-Beispiel-Code aus der Webanwendung 25
Abbildung 18.2: Glyphicons Code aus der Webanwendung 25
Abbildung 19: „:hover“-Eigenschaft für Buttons 26
Abbildung 19.1: „:hover“-CSS-Code aus der Webanwendung 26
Abbildung 20: „Sweetalert“-Dialogfenster aus der Webanwendung 27
Abbildung 20.1: „:Sweetalert“-Code aus der Webanwendung 27
Abbildung 21: Paper-Prototyps 28
Abbildung 22: Einloggen 29
Abbildung 22.1: Admin-Dashboard Bildschirm 29
Abbildung 23: Use Case Diagramm 30
Abbildung 24: Sequence Diagramm 31
Abbildung 25: Aktivität Diagramm 32
Abbildung 26: DB-Modell 33
Abbildung 27: Startseite der Web-App 34
Abbildung 27.1: Startseite der Web-App (2) 34
Abbildung 28: Login 35
Abbildung 29: Admin-Dashboard 36
Abbildung 30: Fächer verwalten 36
Abbildung 30.1: Abteilungen verwalten 36
Abbildung 30.2: Neue Abteilung hinzufügen 37
Abbildung 30.3: Neues Fach hinzufügen 37
Abbildung 30.4: Vorhandene Abteilung bearbeiten/löschen 37
Abbildung 30.5: Vorhandenes Fach bearbeiten/löschen 37
Abbildung 31: Klasse-Stundenplan verwalten 38
Abbildung 32: Notizen verwalten 38
Abbildung 33: Prüfungen-Stundenplan verwalten 38
Abbildung 34: Admin-Bereich 39
Abbildung 34.1: Startseite des Admin-Bereichs 39
Abbildung 35: Gruppen verwalten 40
Abbildung 35.1: Gruppen/Mitglieder mailen 40
Abbildung 36: Gruppen-Mitglieder verwalten 40
Abbildung 37: Bibliothek-Verwaltung 41
Abbildung 37.1: Bibliothek-Verwaltung 41
Abbildung 37.2: Buch hinzufügen 42
Abbildung 38: CSV-Dateien importieren 42
Abbildung 38.1: Import einstellen 43
Abbildung 38.2: Daten überprüfen 43
Abbildung 38.3: Import ausführen 43
Abbildung 39: Backup erstellen 44
Abbildung 39.1: Backup erstellen (2) 44
Abbildung 40: Datensätze übertragen 45
Abbildung 41: Schüler-Dashboard 45
Abbildung 42: Klasse-Stundenplan verwalten 46
Abbildung 43: Notizen verwalten 46
Abbildung 44: Prüfungen-Stundenplan verwalten 46
Abbildung 45: Persönliche Stundenplan verwalten 47
Abbildung 46: Alle Termine auf eine Ansicht 47
Abbildung 47: Masßumwandlungen 48
Abbildung 47.1: Masßumwandlungen 48
Abbildung 48: Bibliothek für Schüler 49
Abbildung 48.1: Buch-Beschreibunsseite 49
Abbildung 49: Professor-Dashboard 50
Abbildung 50: Persönliche Stundenplan verwalten 50
Abbildung 50.1: Notizen verwalten 51
Abbildung 50.2: Prüfungen-Stundenplan verwalten 51
Abbildung 51: Alle Termine auf eine Ansicht 51
Abbildung 51.1: Bearbeiten und/oder löschen einer Datensatz 52

Abbildungsverzeichnis
Abkürzungsverzeichnis
1. Einleitung
1.1 Motivation 
1.2 Zielsetzung
1.3 Aufbau der Arbeit
2. Konzeption und Anforderungen
2.1 Projektstrukturplan
2.2 Erstellung der App Konzeption 
 2.2.1 Definition der App
     2.2.2 Wer sind die Nutzer
     2.2.3 Ideen Brainstorming
2.3 Funktionalitätsliste
	2.3.1 Must-have Funktionen 
	2.3.2 Should-have Funktionen
	2.3.3 Could-have Funktionen  
2.4. Funktionale Anforderungen
3. Projektmanagement 
3.1 Projektplan (noch bearbeiten)
3.2 Methodology Development Model
4. Theoretische Gundlagen 
4.1 Ansätze zur Entwicklung einer Web-Anwendung
4.1.1 LAMP – der Klassiker
4.1.2 MEAN – die moderne Architektur für Einzelseiten-Webanwendungen
4.1.3 WISA – der Microsoft-Stack
4.2 Verwendete Technologien zur Entwicklung der Web-Applikation:  
5. Design-Konzeption
5.1. Typographie
5.2. Farbe
     5.3. Icons
       5.4. Buttons
       5.4. Dialogfenster
       5.6. UI Konzept
5.6.1. Paper-Prototyps
5.6.2. Elektronische Mockups
6. Die Anwendung
5.1 Use Case Diagramm
5.2 Sequence Diagramm 
5.3 Aktivität Diagramm 
5.6 Datenbank
5.7 Vorstellung der App 
5.7.1 Für einen Administrator
   5.7.1.1 Dashboard
   5.7.1.2 Admin-Bereich
5.7.2 Für einen Professor
   5.7.2.1 Dashboard
6. Technical Review
7 Fazit
8 Literaturverzeichnis

6.5.2 Für einen Schüler
6.5.2.1 Dashboard
6.5.3 Für einen Professor
6.5.3.1 Dashboard





Erste Seite:

Ralf-Oliver Mevius, Karsten Weronek
Die vorliegende Arbeit wurde im Zeitraum von 25.02.2021 bis 06.04.2021, unter
der Leitung vom Prof. Dr. Eicke Godehardt und
           der Prof. Dr. Alexander Mützel angefertigt
Achrafeddine Ahnin
E-Mail: achrafaaroui@gmail.com
Fried.-Wilh.-Von-Steuben-Str.90-C224
60480 Frankfurt am Main
Matrikelnummer: 1235395

Zweite Seite:

Achrafeddine Ahnin
E-Mail: achrafaaroui@gmail.com
Fried.-Wilh.-Von-Steuben-Str.90-C224
60480 Frankfurt am Main
Matrikelnummer: 1235395

     Frankfurt, 04.05.2021

SweetAlert.js als alert() Alternative – Schöne Fehlermeldungen mit JavaScript 20
JavaScript/Tutorials/Eigene modale Dialogfenster
JavaScript - Dialog Boxes
Wie kann man Buttons mit CSS gestalten
Bootstrap Glyphicon Components
Was ist CSS – Was sind Cascading-Style-Sheets?
Was ist CSS? Definition und Anwendung
GitLab: Was ist das und was kann das?
Komodo Edit
JavaScript Guide: Was ist JavaScript?

Pascal Bajorat 30. März 2015   20
o.V 19. Januar 2021
o.V 2021
o.V
o.V 2021
o.V 2021
o.v  09.11.20
Viktor PetersTools 10.12.2019 
o.V 17.09.2015
Jan-Dirk 19.11.2019
Jan-Dirk 21.03.2019
o.V 2021
11.10.2018Autor / Redakteur: Tutanch / Nico Litzel
o.V Verfasst am 10. Januar 2018
06.09.2018 19:12 | von Yannick Brunken
o.V  01.02.18
o.V  10.03.20
o.V 2021
o.V Donnerstag, 03. Oktober 2019
o.V 2021


























